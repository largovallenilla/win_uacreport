﻿using System;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace UAC_Order_Reports_App
{
    public partial class Mainw : Form
    {
        Excel.Application xlApp;
        Excel.Workbook wbook;
        Excel.Worksheet wsheet;
        Excel.Range xlrange;

        public Mainw()
        {
            InitializeComponent();
        }

        private void tb_order_TextChanged(object sender, EventArgs e)
        {
            tb_date.Text = "";
            tb_whno.Text = "";
        }

        private void tb_date_TextChanged(object sender, EventArgs e)
        {
            tb_order.Text = "";
            tb_whno.Text = "";
        }

        private void tb_whno_TextChanged(object sender, EventArgs e)
        {
            tb_date.Text = "";
            tb_order.Text = "";
        }

        private void btn_search_Click(object sender, EventArgs e)
        {

        }

        private void btn_view_Click(object sender, EventArgs e)
        {

        }

        private void btn_pick_Click(object sender, EventArgs e)
        {

        }

        private void btn_pack_Click(object sender, EventArgs e)
        {

        }

        private void repBtn_pick_Click(object sender, EventArgs e)
        {

        }

        private void repBtn_pack_Click(object sender, EventArgs e)
        {

        }

        private void repBtn_both_Click(object sender, EventArgs e)
        {

        }

        private void ckb_oneDate_CheckedChanged(object sender, EventArgs e)
        {
            if(ckb_oneDate.Checked)
            {
                dtp_to.Enabled = false;
            }
            else
            {
                dtp_to.Enabled = true;
            }
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog xldlg = new OpenFileDialog();

            xldlg.Multiselect = false;
            xldlg.ShowDialog(this);

            string wb_dir = xldlg.FileName;

            try
            {
                if (wb_dir != "")
                {
                    xlApp = new Excel.Application();
                    wbook = xlApp.Workbooks.Open(wb_dir);
                    wsheet = wbook.Sheets[1];
                    xlrange = wsheet.UsedRange;
                }

                for (int i = 1; i < xlrange.Rows.Count; i++)
                {
                    dgv_Orders.Rows.Add();

                    dgv_Orders[5, i-1].Value = "Pending";

                    for (int j = 1; j <= xlrange.Columns.Count; j++)
                    {
                        if (xlrange.Cells[i,j] != null && xlrange.Cells[i,j].Value2 != null)
                        {
                            dgv_Orders[j-1, i-1].Value = xlrange.Cells[i, j].Value2;
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
