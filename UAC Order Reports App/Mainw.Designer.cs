﻿namespace UAC_Order_Reports_App
{
    partial class Mainw
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_Orders = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_view = new System.Windows.Forms.Button();
            this.btn_pick = new System.Windows.Forms.Button();
            this.btn_pack = new System.Windows.Forms.Button();
            this.tb_order = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.repBtn_pick = new System.Windows.Forms.Button();
            this.repBtn_pack = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.repBtn_both = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_date = new System.Windows.Forms.TextBox();
            this.btn_search = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_whno = new System.Windows.Forms.TextBox();
            this.Order = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Picker = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Packer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dtp_from = new System.Windows.Forms.DateTimePicker();
            this.dtp_to = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.ckb_oneDate = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Orders)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_Orders
            // 
            this.dgv_Orders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Orders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Order,
            this.WH,
            this.Date,
            this.Picker,
            this.Packer,
            this.Status});
            this.dgv_Orders.Location = new System.Drawing.Point(12, 226);
            this.dgv_Orders.Name = "dgv_Orders";
            this.dgv_Orders.Size = new System.Drawing.Size(760, 203);
            this.dgv_Orders.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStripMenuItem,
            this.exportToolStripMenuItem,
            this.toolStripSeparator1,
            this.aboutToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
            this.toolStripMenuItem1.Text = "File";
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.importToolStripMenuItem.Text = "Import";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.importToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exportToolStripMenuItem.Text = "Export";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(177, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // btn_view
            // 
            this.btn_view.Location = new System.Drawing.Point(16, 197);
            this.btn_view.Name = "btn_view";
            this.btn_view.Size = new System.Drawing.Size(75, 23);
            this.btn_view.TabIndex = 2;
            this.btn_view.Text = "View";
            this.btn_view.UseVisualStyleBackColor = true;
            this.btn_view.Click += new System.EventHandler(this.btn_view_Click);
            // 
            // btn_pick
            // 
            this.btn_pick.Location = new System.Drawing.Point(97, 197);
            this.btn_pick.Name = "btn_pick";
            this.btn_pick.Size = new System.Drawing.Size(75, 23);
            this.btn_pick.TabIndex = 3;
            this.btn_pick.Text = "Set Pick";
            this.btn_pick.UseVisualStyleBackColor = true;
            this.btn_pick.Click += new System.EventHandler(this.btn_pick_Click);
            // 
            // btn_pack
            // 
            this.btn_pack.Location = new System.Drawing.Point(174, 197);
            this.btn_pack.Name = "btn_pack";
            this.btn_pack.Size = new System.Drawing.Size(75, 23);
            this.btn_pack.TabIndex = 4;
            this.btn_pack.Text = "Set Pack";
            this.btn_pack.UseVisualStyleBackColor = true;
            this.btn_pack.Click += new System.EventHandler(this.btn_pack_Click);
            // 
            // tb_order
            // 
            this.tb_order.Location = new System.Drawing.Point(62, 50);
            this.tb_order.Name = "tb_order";
            this.tb_order.Size = new System.Drawing.Size(308, 25);
            this.tb_order.TabIndex = 5;
            this.tb_order.TextChanged += new System.EventHandler(this.tb_order_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ckb_oneDate);
            this.groupBox1.Controls.Add(this.dtp_to);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.dtp_from);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.repBtn_both);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.repBtn_pack);
            this.groupBox1.Controls.Add(this.repBtn_pick);
            this.groupBox1.Location = new System.Drawing.Point(376, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(396, 207);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Reports";
            // 
            // repBtn_pick
            // 
            this.repBtn_pick.Location = new System.Drawing.Point(160, 108);
            this.repBtn_pick.Name = "repBtn_pick";
            this.repBtn_pick.Size = new System.Drawing.Size(75, 23);
            this.repBtn_pick.TabIndex = 10;
            this.repBtn_pick.Text = "Generate";
            this.repBtn_pick.UseVisualStyleBackColor = true;
            this.repBtn_pick.Click += new System.EventHandler(this.repBtn_pick_Click);
            // 
            // repBtn_pack
            // 
            this.repBtn_pack.Location = new System.Drawing.Point(160, 137);
            this.repBtn_pack.Name = "repBtn_pack";
            this.repBtn_pack.Size = new System.Drawing.Size(75, 23);
            this.repBtn_pack.TabIndex = 11;
            this.repBtn_pack.Text = "Generate";
            this.repBtn_pack.UseVisualStyleBackColor = true;
            this.repBtn_pack.Click += new System.EventHandler(this.repBtn_pack_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 19);
            this.label1.TabIndex = 12;
            this.label1.Text = "Picks report by date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 19);
            this.label2.TabIndex = 13;
            this.label2.Text = "Packs report by date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(148, 19);
            this.label3.TabIndex = 14;
            this.label3.Text = "General report by date";
            // 
            // repBtn_both
            // 
            this.repBtn_both.Location = new System.Drawing.Point(160, 166);
            this.repBtn_both.Name = "repBtn_both";
            this.repBtn_both.Size = new System.Drawing.Size(75, 23);
            this.repBtn_both.TabIndex = 15;
            this.repBtn_both.Text = "Generate";
            this.repBtn_both.UseVisualStyleBackColor = true;
            this.repBtn_both.Click += new System.EventHandler(this.repBtn_both_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 19);
            this.label4.TabIndex = 8;
            this.label4.Text = "Search By:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 19);
            this.label5.TabIndex = 9;
            this.label5.Text = "PO #";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 86);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 19);
            this.label6.TabIndex = 11;
            this.label6.Text = "Date";
            // 
            // tb_date
            // 
            this.tb_date.Location = new System.Drawing.Point(62, 83);
            this.tb_date.Name = "tb_date";
            this.tb_date.Size = new System.Drawing.Size(308, 25);
            this.tb_date.TabIndex = 10;
            this.tb_date.TextChanged += new System.EventHandler(this.tb_date_TextChanged);
            // 
            // btn_search
            // 
            this.btn_search.Location = new System.Drawing.Point(16, 145);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(69, 27);
            this.btn_search.TabIndex = 12;
            this.btn_search.Text = "Search";
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 19);
            this.label7.TabIndex = 14;
            this.label7.Text = "WH #";
            // 
            // tb_whno
            // 
            this.tb_whno.Location = new System.Drawing.Point(62, 114);
            this.tb_whno.Name = "tb_whno";
            this.tb_whno.Size = new System.Drawing.Size(308, 25);
            this.tb_whno.TabIndex = 13;
            this.tb_whno.TextChanged += new System.EventHandler(this.tb_whno_TextChanged);
            // 
            // Order
            // 
            this.Order.HeaderText = "Order No";
            this.Order.MinimumWidth = 120;
            this.Order.Name = "Order";
            this.Order.Width = 120;
            // 
            // WH
            // 
            this.WH.HeaderText = "WH No";
            this.WH.MinimumWidth = 80;
            this.WH.Name = "WH";
            this.WH.Width = 80;
            // 
            // Date
            // 
            this.Date.HeaderText = "Date";
            this.Date.MinimumWidth = 120;
            this.Date.Name = "Date";
            this.Date.Width = 120;
            // 
            // Picker
            // 
            this.Picker.HeaderText = "Picker";
            this.Picker.MinimumWidth = 120;
            this.Picker.Name = "Picker";
            this.Picker.Width = 120;
            // 
            // Packer
            // 
            this.Packer.HeaderText = "Packer";
            this.Packer.MinimumWidth = 120;
            this.Packer.Name = "Packer";
            this.Packer.Width = 120;
            // 
            // Status
            // 
            this.Status.HeaderText = "Status";
            this.Status.MinimumWidth = 120;
            this.Status.Name = "Status";
            this.Status.Width = 120;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 19);
            this.label8.TabIndex = 16;
            this.label8.Text = "Set Date";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 43);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 19);
            this.label9.TabIndex = 17;
            this.label9.Text = "From";
            // 
            // dtp_from
            // 
            this.dtp_from.Location = new System.Drawing.Point(53, 40);
            this.dtp_from.Name = "dtp_from";
            this.dtp_from.Size = new System.Drawing.Size(242, 25);
            this.dtp_from.TabIndex = 18;
            // 
            // dtp_to
            // 
            this.dtp_to.Location = new System.Drawing.Point(53, 73);
            this.dtp_to.Name = "dtp_to";
            this.dtp_to.Size = new System.Drawing.Size(242, 25);
            this.dtp_to.TabIndex = 20;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 76);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 19);
            this.label10.TabIndex = 19;
            this.label10.Text = "From";
            // 
            // ckb_oneDate
            // 
            this.ckb_oneDate.AutoSize = true;
            this.ckb_oneDate.Location = new System.Drawing.Point(301, 43);
            this.ckb_oneDate.Name = "ckb_oneDate";
            this.ckb_oneDate.Size = new System.Drawing.Size(89, 23);
            this.ckb_oneDate.TabIndex = 21;
            this.ckb_oneDate.Text = "Same Day";
            this.ckb_oneDate.UseVisualStyleBackColor = true;
            this.ckb_oneDate.CheckedChanged += new System.EventHandler(this.ckb_oneDate_CheckedChanged);
            // 
            // Mainw
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 441);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tb_whno);
            this.Controls.Add(this.btn_search);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tb_date);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tb_order);
            this.Controls.Add(this.btn_pack);
            this.Controls.Add(this.btn_pick);
            this.Controls.Add(this.btn_view);
            this.Controls.Add(this.dgv_Orders);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(800, 480);
            this.MinimumSize = new System.Drawing.Size(800, 480);
            this.Name = "Mainw";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Order Reports";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Orders)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_Orders;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Button btn_view;
        private System.Windows.Forms.Button btn_pack;
        private System.Windows.Forms.TextBox tb_order;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button repBtn_pack;
        private System.Windows.Forms.Button repBtn_pick;
        private System.Windows.Forms.Button repBtn_both;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_date;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_whno;
        private System.Windows.Forms.Button btn_pick;
        private System.Windows.Forms.DataGridViewTextBoxColumn Order;
        private System.Windows.Forms.DataGridViewTextBoxColumn WH;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Picker;
        private System.Windows.Forms.DataGridViewTextBoxColumn Packer;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DateTimePicker dtp_from;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox ckb_oneDate;
        private System.Windows.Forms.DateTimePicker dtp_to;
        private System.Windows.Forms.Label label10;
    }
}

